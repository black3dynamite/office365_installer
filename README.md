
# Microsoft Office 365
This script will download and extract Office 365 Deployment Tool contents and then install Office 365 ProPlus 32-bit or 64-bit.

# Windows version tested on...
Windows 10 Pro 1607 64-bit (OS Build 14393.0)  
Windows 10 Pro 1803 64-bit (OS Build 17134.648)  
Windows 10 Pro 1809 64-bit (OS Build 17763.316)  
Windows 10 Pro 1903 64-bit (OS Build 18362.239)  

# Requirements
CMD.exe and PowerShell.exe needs to be run as Administrator

# CMD.exe
```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; "iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/black3dynamite/office365_installer/raw/master/office365_install.ps1'))"
```
# Requires PowerShell v2+
```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/black3dynamite/office365_installer/raw/master/office365_install.ps1'))
```

# Requires PowerShell v3+
```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; Set-ExecutionPolicy Bypass -Scope Process -Force; iwr https://gitlab.com/black3dynamite/office365_installer/raw/master/office365_install.ps1 -UseBasicParsing | iex
```