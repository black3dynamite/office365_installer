<#
.SYNOPSIS
Short description

.DESCRIPTION
Long description

.EXAMPLE
An example

.NOTES
General notes
#>

function Install_Office365 () {
    # If $ODT_Path does not exist and then create the path
    # Download and extract Office365DeploymentTool
    # Create a clean configuration.xml file even 
    if (($ODT_PathValid) -eq $false) {
        New-Item `
            -Path $ODT_Path `
            -ItemType Directory -Force

        # Download Office365DeploymentTool
        Start-BitsTransfer `
            -Source $URL_ODTFile `
            -Destination $Output_ODTFile

        # Extract OfficeDeploymentTool files
        Push-Location $ODT_Path
        Start-Process `
            -FilePath $Output_ODTFile `
            -ArgumentList "/extract:$ODT_Path /log:$ODT_Path\Office365Install.log /quiet /norestart" `
            -Wait
        Pop-Location

        # Create a clean configuration.xml file
        New-Item `
            -Path $ODT_Path `
            -Name configuration.xml `
            -ItemType File -Force | Out-Null
    }

    do {
        Write-Host ""
        Write-Host "A - Office 365 ProPlus x32"
        Write-Host "B - Office 365 ProPlus x64"
        Write-Host ""
        Write-Host "X - Exit"
        Write-Host ""
        Write-Host -NoNewline "Type your choice and press Enter: "

        $Choice = Read-Host

        Write-Host ""

        $Ok = $Choice -match '^[abx]+$'

        if (-not $Ok) { Write-Host "Invalid selection" }
    } until ($Ok)

    switch -Regex ($Choice) {
        "A" {
            Write-Host "Installing Office 365 ProPlus 32-bit..."
            Install_Office365ProPlus32
        }
        "B" {
            Write-Host "Installing Office 365 ProPlus 64-bit..."
            Install_Office365ProPlus64
        }
        "X" {
            Write-Host "Goodbye!"

            Cleanup_Office365Installer
        }
    }
}

function Install_Office365ProPlus32 () {
    # Add configuration settings for Office 365 ProPlus 32-bit in the configuration.xml file
    Add-Content -Path $ODT_Path\configuration.xml -Value '<Configuration>'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <Add OfficeClientEdition="32" Channel="Monthly">'
    Add-Content -Path $ODT_Path\configuration.xml -Value '    <Product ID="O365ProPlusRetail">'
    Add-Content -Path $ODT_Path\configuration.xml -Value '      <Language ID="MatchOS" />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '    </Product>'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  </Add>'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <Updates Enabled="TRUE" />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <RemoveMSI />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <Display Level="None" AcceptEULA="TRUE" />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '</Configuration>'

    # Installing Office 365 ProPlus 32-bit
    Write-Host ""
    Push-Location $ODT_Path
    Start-Process `
        -FilePath "setup.exe" `
        -ArgumentList "/configure configuration.xml" `
        -Wait -WindowStyle Hidden
    Pop-Location

    Write-Host ""
    Write-Host "Finished installing Office 365 ProPlus 32-bit"
    Write-Host ""

    Cleanup_Office365Installer
}

function Install_Office365ProPlus64 () {
    # Add configuration settings for Office 365 ProPlus 64-bit in the configuration.xml file
    Add-Content -Path $ODT_Path\configuration.xml -Value '<Configuration>'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <Add OfficeClientEdition="64" Channel="Monthly">'
    Add-Content -Path $ODT_Path\configuration.xml -Value '    <Product ID="O365ProPlusRetail">'
    Add-Content -Path $ODT_Path\configuration.xml -Value '      <Language ID="MatchOS" />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '    </Product>'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  </Add>'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <Updates Enabled="TRUE" />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <RemoveMSI />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '  <Display Level="None" AcceptEULA="TRUE" />'
    Add-Content -Path $ODT_Path\configuration.xml -Value '</Configuration>'

    # Installing Office 365 ProPlus 64-bit
    Write-Host ""
    Push-Location $ODT_Path
    Start-Process `
        -FilePath "setup.exe" `
        -ArgumentList "/configure configuration.xml" `
        -Wait -WindowStyle Hidden
    Pop-Location

    Write-Host ""
    Write-Host "Finished installing Office 365 ProPlus 64-bit"
    Write-Host ""

    Cleanup_Office365Installer
}


# Cleanup Office 365 Installer
function Cleanup_Office365Installer () {
    # Remove $ODTPATH and its contents
    Write-Host ""
    Write-Host ""
    Write-Host "Removing $ODT_Path and its contents..."
    Start-Sleep -Seconds 30
    Write-Host ""
    Write-Host ""
    Remove-Item `
        -Path $ODT_Path `
        -Recurse -Force
}

# Test URI
# https://www.petri.com/testing-uris-urls-powershell
#requires -version 4.0
 Function Test-URI {
    <#
    .Synopsis
    Test a URI or URL
    .Description
    This command will test the validity of a given URL or URI that begins with either http or https. The default behavior is to write a Boolean value to the pipeline. But you can also ask for more detail.
     
    Be aware that a URI may return a value of True because the server responded correctly. For example this will appear that the URI is valid.
     
    test-uri -uri http://files.snapfiles.com/localdl936/CrystalDiskInfo7_2_0.zip
     
    But if you look at the test in detail:
     
    ResponseUri   : http://files.snapfiles.com/localdl936/CrystalDiskInfo7_2_0.zip
    ContentLength : 23070
    ContentType   : text/html
    LastModified  : 1/19/2015 11:34:44 AM
    Status        : 200
     
    You'll see that the content type is Text and most likely a 404 page. By comparison, this is the desired result from the correct URI:
     
    PS C:\> test-uri -detail -uri http://files.snapfiles.com/localdl936/CrystalDiskInfo6_3_0.zip
     
    ResponseUri   : http://files.snapfiles.com/localdl936/CrystalDiskInfo6_3_0.zip
    ContentLength : 2863977
    ContentType   : application/x-zip-compressed
    LastModified  : 12/31/2014 1:48:34 PM
    Status        : 200
     
    .Example
    PS C:\> test-uri https://www.petri.com
    True
    .Example
    PS C:\> test-uri https://www.petri.com -detail
     
    ResponseUri   : https://www.petri.com/
    ContentLength : -1
    ContentType   : text/html; charset=UTF-8
    LastModified  : 1/19/2015 12:14:57 PM
    Status        : 200
    .Example
    PS C:\> get-content D:\temp\uris.txt | test-uri -Detail | where { $_.status -ne 200 -OR $_.contentType -notmatch "application"}
     
    ResponseUri   : http://files.snapfiles.com/localdl936/CrystalDiskInfo7_2_0.zip
    ContentLength : 23070
    ContentType   : text/html
    LastModified  : 1/19/2015 11:34:44 AM
    Status        : 200
     
    ResponseURI   : http://download.bleepingcomputer.com/grinler/rkill
    ContentLength : 
    ContentType   : 
    LastModified  : 
    Status        : 404
     
    Test a list of URIs and filter for those that are not OK or where the type is not an application.
    .Notes
    Last Updated: January 19, 2015
    Version     : 1.0
     
    Learn more about PowerShell:
    http://jdhitsolutions.com/blog/essential-powershell-resources/
     
      ****************************************************************
      * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
      * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
      * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
      * DO NOT USE IT OUTSIDE OF A SECURE, TEST SETTING.             *
      ****************************************************************
     
    .Link
    Invoke-WebRequest
    #>
     
    [cmdletbinding(DefaultParameterSetName="Default")]
    Param(
    [Parameter(Position=0,Mandatory,HelpMessage="Enter the URI path starting with HTTP or HTTPS",
    ValueFromPipeline,ValueFromPipelineByPropertyName)]
    [ValidatePattern( "^(http|https)://" )]
    [Alias("url")]
    [string]$URI,
    [Parameter(ParameterSetName="Detail")]
    [Switch]$Detail,
    [ValidateScript({$_ -ge 0})]
    [int]$Timeout = 30
    )
     
    Begin {
        Write-Verbose -Message "Starting $($MyInvocation.Mycommand)" 
        Write-Verbose -message "Using parameter set $($PSCmdlet.ParameterSetName)" 
    } #close begin block
     
    Process {
     
        Write-Verbose -Message "Testing $uri"
        Try {
         #hash table of parameter values for Invoke-Webrequest
         $paramHash = @{
         UseBasicParsing = $True
         DisableKeepAlive = $True
         Uri = $uri
         Method = 'Head'
         ErrorAction = 'stop'
         TimeoutSec = $Timeout
        }
     
        $test = Invoke-WebRequest @paramHash
     
         if ($Detail) {
            $test.BaseResponse | 
            Select-Object ResponseURI,ContentLength,ContentType,LastModified,
            @{Name="Status";Expression={$Test.StatusCode}}
         } #if $detail
         else {
           if ($test.statuscode -ne 200) {
                #it is unlikely this code will ever run but just in case
                Write-Verbose -Message "Failed to request $uri"
                write-Verbose -message ($test | out-string)
                $False
             }
             else {
                $True
             }
         } #else quiet
         
        }
        Catch {
          #there was an exception getting the URI
          write-verbose -message $_.exception
          if ($Detail) {
            #most likely the resource is 404
            $objProp = [ordered]@{
            ResponseURI = $uri
            ContentLength = $null
            ContentType = $null
            LastModified = $null
            Status = 404
            }
            #write a matching custom object to the pipeline
            New-Object -TypeName psobject -Property $objProp
     
            } #if $detail
          else {
            $False
          }
        } #close Catch block
    } #close Process block
     
    End {
        Write-Verbose -Message "Ending $($MyInvocation.Mycommand)"
    } #close end block
     
    } #close Test-URI Function

Import-Module BitsTransfer

Clear-Host

# Setup variables
# https://stackoverflow.com/a/21457366
# https://community.spiceworks.com/topic/555978-powershell-download-file-from-redirect-url?page=1#entry-3642558
$SourceODT = (Invoke-WebRequest -Uri "https://www.microsoft.com/en-us/download/confirmation.aspx?id=49117" -MaximumRedirection 0 -UseBasicParsing)
$URL_ODTFile = $SourceODT.Links | Where-Object {$_."data-bi-cN" -eq "click here to download manually"} | Select-Object -ExpandProperty href

$ODT_Path = "C:\ODT"
$ODTFile = "officedeploymenttool.exe"
$Output_ODTFile = "$ODT_Path\$ODTFile"

$URL_ODTFileValid = Test-URI -uri $URL_ODTFile
$ODT_PathValid = Test-Path -Path $ODT_Path

# 3 ways to download OfficeDeploymentTool
#Start-BitsTransfer -Source $URL_ODTFile -Destination $Output_ODTFile
#Invoke-WebRequest -Uri $URL_ODTFile -OutFile $Output_ODTFile
#(New-Object System.Net.WebClient).DownloadFile($URL_ODTFile, $Output_ODTFile)

if ($URL_ODTFileValid) {
    Write-Host "URL is valid"
    Write-Host ""
    Install_Office365
}
else {
    Write-Host "URL is invalid or not found"
    Write-Host ""
    Write-Host ""
}